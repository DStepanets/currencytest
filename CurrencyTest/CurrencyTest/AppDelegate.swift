//
//  AppDelegate.swift
//  CurrencyTest
//
//  Created by Dmitry Stepanets on 11/07/2019.
//  Copyright © 2019 Dmitry Stepanets. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        print("Realm: \(Realm.Configuration.defaultConfiguration.fileURL!)")
        return true
    }
}

