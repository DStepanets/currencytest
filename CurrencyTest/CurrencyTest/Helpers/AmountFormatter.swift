//
//  AmountFormatter.swift
//  CurrencyTest
//
//  Created by Dmitry Stepanets on 12/07/2019.
//  Copyright © 2019 Dmitry Stepanets. All rights reserved.
//

import UIKit

struct AmountFormatter {
    static let fmt: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.generatesDecimalNumbers = true
        formatter.decimalSeparator = "."
        formatter.minimumIntegerDigits = 1
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        
        return formatter
    }()
}
