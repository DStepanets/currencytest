//
//  Array+Duplicate.swift
//  CurrencyTest
//
//  Created by Dmitry Stepanets on 12/07/2019.
//  Copyright © 2019 Dmitry Stepanets. All rights reserved.
//

import UIKit

extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()
        
        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }
    
    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}
