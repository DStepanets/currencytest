//
//  CurrencySymbol.swift
//  CurrencyTest
//
//  Created by Dmitry Stepanets on 11/07/2019.
//  Copyright © 2019 Dmitry Stepanets. All rights reserved.
//

import UIKit
import RealmSwift

class CurrencySymbol: Object {
    @objc dynamic var code:String = ""
    @objc dynamic var name:String = ""
    
    override static func primaryKey() -> String? {
        return "code"
    }
    
    convenience init(withCode code:String, name:String) {
        self.init()
        self.code = code
        self.name = name
    }
}
