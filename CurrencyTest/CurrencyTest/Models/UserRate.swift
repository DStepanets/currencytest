//
//  UserRate.swift
//  CurrencyTest
//
//  Created by Dmitry Stepanets on 11/07/2019.
//  Copyright © 2019 Dmitry Stepanets. All rights reserved.
//

import UIKit
import RealmSwift

class UserRate: Object {
    //Private
    @objc private(set) dynamic var userRateId:String = "N_N"
    
    //Public
    @objc dynamic var baseCurrency:CurrencySymbol?
    @objc dynamic var toCurrency:CurrencySymbol?
    @objc dynamic var amount:Double = 0.0
    
    override static func primaryKey() -> String? {
        return "userRateId"
    }

    convenience init(withBaseRate baseCurrency:CurrencySymbol, targetCurrency:CurrencySymbol, amount:Double) {
        self.init()
        
        userRateId = "\(baseCurrency.code)_\(targetCurrency.code)"
        self.baseCurrency = baseCurrency
        self.toCurrency = targetCurrency
        self.amount = amount
    }
}
