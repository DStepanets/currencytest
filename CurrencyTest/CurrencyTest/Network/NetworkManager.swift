//
//  NetworkManager.swift
//  CurrencyTest
//
//  Created by Dmitry Stepanets on 11/07/2019.
//

import UIKit

class NetworkManager: NSObject {
    private let kHost = "https://api.exchangeratesapi.io"
    
    func makeRequest(atPath:String, params:[String:Any]?, completion:@escaping(_ error:String?,_ jsonDict:[String:Any]?) -> Void) {
        var pathString = "\(atPath)"
        if let requestParams = params {
            for (index, paramDict) in requestParams.enumerated() {
                if index == 0 {
                    pathString += "?\(paramDict.key)=\(paramDict.value)"
                    continue
                }
                
                pathString += "&\(paramDict.key)=\(paramDict.value)"
            }
        }
        
        guard let encodedPath = pathString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            completion("Failed to encode path", nil)
            return
        }
        
        guard let url = URL(string: "\(kHost)\(encodedPath)") else {
            completion("Invalid URL", nil)
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                completion(error?.localizedDescription, nil)
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                guard 200...299 ~= httpResponse.statusCode else {
                    completion("Request failed. Code: \(httpResponse.statusCode)", nil)
                    return
                }
            }
            
            do {
                guard let responseData = data else {
                    completion("Response data is empty", nil)
                    return
                }
                
                guard let jsonDict = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) as? [String:Any] else {
                    completion("Network error. Incorrect data format", nil)
                    return
                }
                
                if let errorInfo = jsonDict["error"] as? String {
                    completion("Request failed. \(errorInfo)", nil)
                    return
                }
                
                completion(nil, jsonDict)
            }
            catch {
                completion(error.localizedDescription, nil)
            }
        }
        dataTask.resume()
    }
}
