//
//  RatesProvider.swift
//  CurrencyTest
//
//  Created by Dmitry Stepanets on 11/07/2019.
//  Copyright © 2019 Dmitry Stepanets. All rights reserved.
//

import UIKit
import RealmSwift

class RatesProvider: NSObject {
    private let networkManager = NetworkManager()
    
    func getLatestRates(forBaseCurrency base:String,
                        targetCurrencies targetCodes:[String],
                        completion:@escaping (_ error:String?) -> Void) {
        var codesString = ""
        for (index, value) in targetCodes.enumerated() {
            if index == 0 {
                codesString += "\(value)"
                continue
            }
            
            codesString += ",\(value)"
        }
        
        networkManager.makeRequest(atPath: "/latest",
                                   params: ["base":base,
                                            "symbols":codesString]) { (error, jsonDict) in
            guard error == nil else {
                completion(error)
                return
            }
            
            do {
                let realm = try Realm()
                guard let ratesDict = jsonDict?["rates"] as? [String:Double] else {
                    completion("Rates object does't exists")
                    return
                }
                
                guard let baseCurrency = realm.object(ofType: CurrencySymbol.self, forPrimaryKey: base) else {
                    completion("Failed to initialize base currency")
                    return
                }
                
                var userRates = [UserRate]()
                for (key, value) in ratesDict {
                    if let targetCurrency = realm.object(ofType: CurrencySymbol.self, forPrimaryKey: key) {
                        let rate = UserRate(withBaseRate: baseCurrency,
                                            targetCurrency: targetCurrency,
                                            amount: value)
                        userRates.append(rate)
                    }
                }
                
                try realm.write {
                    realm.add(userRates, update: .modified)
                }
                completion(nil)
            }
            catch {
                completion(error.localizedDescription)
            }
        }
    }
}
