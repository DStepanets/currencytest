//
//  CurrencyCell.swift
//  CurrencyTest
//
//  Created by Dmitry Stepanets on 12/07/2019.
//  Copyright © 2019 Dmitry Stepanets. All rights reserved.
//

import UIKit

class CurrencyCell: UITableViewCell {
    @IBOutlet private weak var codeLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    
    var currency:CurrencySymbol? {
        didSet {
            guard currency != nil else  {
                return
            }
            
            codeLabel.text = currency?.code
            nameLabel.text = currency?.name
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        prepareLabels()
    }
}

private extension CurrencyCell {
    func prepareLabels() {
        codeLabel.font = UIFont.systemFont(ofSize: 16)
        
        nameLabel.font = UIFont.systemFont(ofSize: 16)
        nameLabel.textColor = .gray
    }
}
