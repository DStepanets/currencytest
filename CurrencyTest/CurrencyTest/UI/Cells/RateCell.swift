//
//  RateCell.swift
//  CurrencyTest
//
//  Created by Dmitry Stepanets on 11/07/2019.
//  Copyright © 2019 Dmitry Stepanets. All rights reserved.
//

import UIKit

class RateCell: UITableViewCell {
    @IBOutlet private weak var currencyLabel: UILabel!
    @IBOutlet private weak var currencyNameLabel: UILabel!
    @IBOutlet private weak var amountLabel: UILabel!
    @IBOutlet private weak var symbolsLabel: UILabel!
    
    var userRate:UserRate? {
        didSet {
            guard
                let rate = userRate,
                let base = userRate?.baseCurrency,
                let target = userRate?.toCurrency
                else {
                    return
            }
            
            currencyLabel.text = "1 \(base.code)"
            currencyNameLabel.text = base.name
            amountLabel.text = AmountFormatter.fmt.string(from: NSNumber(value: rate.amount))
            symbolsLabel.text = "\(target.name) • \(target.code)"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        prepareLabels()
    }
}

private extension RateCell {
    func prepareLabels() {
        currencyLabel.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        
        currencyNameLabel.font = UIFont.systemFont(ofSize: 14)
        currencyNameLabel.textColor = .gray
        
        amountLabel.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        
        symbolsLabel.font = UIFont.systemFont(ofSize: 14)
        symbolsLabel.textColor = .gray
    }
}
