//
//  CurrencyViewController.swift
//  CurrencyTest
//
//  Created by Dmitry Stepanets on 12/07/2019.
//  Copyright © 2019 Dmitry Stepanets. All rights reserved.
//

import UIKit

enum CurrencyControllerType {
    case first
    case second
}

class CurrencyViewController: UITableViewController {
    private var currencyViewModel:CurrencyViewModel?
    private var controllerType:CurrencyControllerType = .first
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareTableView()
    }
    
    func configure(withViewModel viewModel:CurrencyViewModel, controllerType:CurrencyControllerType) {
        self.currencyViewModel = viewModel
        self.controllerType = controllerType
    }
}

// MARK: - Prepare
private extension CurrencyViewController {
    func prepareTableView() {
        self.tableView.tableFooterView = UIView()
    }
}


// MARK: - UITableViewDataSource
extension CurrencyViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencyViewModel?.currencies?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "currencyCell", for: indexPath) as! CurrencyCell
        cell.currency = currencyViewModel?.currencies?[indexPath.row]
       
        return cell
    }
}

// MARK: - UITableViewDelegate
extension CurrencyViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        guard
            let viewModel = currencyViewModel,
            let currencies = viewModel.currencies
            else {
                return
        }
        
        switch controllerType {
        case .first:
            viewModel.baseCurrencyCode = currencies[indexPath.row].code
            
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            guard let currencyViewController = storyboard.instantiateViewController(withIdentifier: "\(CurrencyViewController.self)") as? CurrencyViewController  else {
                return
            }
            
            currencyViewController.title = "Second currency"
            currencyViewController.configure(withViewModel: viewModel, controllerType: .second)
            self.navigationController?.pushViewController(currencyViewController, animated: true)
            break
        case .second:
            viewModel.targetCurrencyCode = currencies[indexPath.row].code
            viewModel.saveRate {[weak self] (error) in
                if let error = error {
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Error",
                                                      message: error,
                                                      preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
                        self?.present(alert, animated: true)
                    }
                }
                
                self?.navigationController?.popToRootViewController(animated: true)
            }
            break
        }
    }
}
