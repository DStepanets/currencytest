//
//  RatesViewController.swift
//  CurrencyTest
//
//  Created by Dmitry Stepanets on 11/07/2019.
//  Copyright © 2019 Dmitry Stepanets. All rights reserved.
//

import UIKit
import SnapKit

class RatesViewController: UITableViewController {
    //Private
    @IBOutlet private weak var addButton: UIBarButtonItem!
    private let rateViewModel = RatesViewModel()
    private let noRatesLabel = UILabel()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rateViewModel.delegate = self
        prepareTableView()
        prepareLabel()
        preapreButtons()
        showLabelIfNeeded()
        rateViewModel.updateRates()
    }
    
    @objc private func handleAddButton() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let currencyViewController = storyboard.instantiateViewController(withIdentifier: "\(CurrencyViewController.self)") as? CurrencyViewController  else {
            return
        }
        
        let currencyViewModel = CurrencyViewModel()
        currencyViewController.title = "Firts currency"
        currencyViewController.configure(withViewModel: currencyViewModel, controllerType: .first)
        self.navigationController?.pushViewController(currencyViewController, animated: true)
    }
    
    @objc private func handleEditButton() {
        if tableView.isEditing {
            tableView.setEditing(false, animated: true)
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit,
                                                               target: self,
                                                               action: #selector(handleEditButton))
        }
        else {
            tableView.setEditing(true, animated: true)
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done,
                                                               target: self,
                                                               action: #selector(handleEditButton))
        }
        
    }
    
    private func showLabelIfNeeded() {
        if let userRates = self.rateViewModel.userRates {
            self.noRatesLabel.isHidden = !userRates.isEmpty
        } else {
            self.noRatesLabel.isHidden = false
        }
    }
    
    @objc private func beginRefreshing() {
        rateViewModel.updateRates()
    }
}

// MARK: - ViewModelDelegate
extension RatesViewController: RatesViewModelDelegate {
    func viewModelDidChange<P>(model: P) where P : ViewModelProtocol {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.showLabelIfNeeded()
        }
    }
    
    func viewModel<P>(model: P, errorHasOccured error: String?) where P : ViewModelProtocol {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Error",
                                          message: error ?? "No description",
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            self.present(alert, animated: true)
        }
    }
    
    func ratesUpdateFinished() {
        DispatchQueue.main.async {
             self.refreshControl?.endRefreshing()
        }
    }
}

// MARK: - Prepare
private extension RatesViewController {
    func prepareTableView() {
        self.refreshControl = UIRefreshControl()
        refreshControl?.attributedTitle = NSAttributedString(string: "Updating...")
        refreshControl?.addTarget(self, action: #selector(beginRefreshing), for: .valueChanged)
        
        self.tableView.tableFooterView = UIView()
        self.tableView.backgroundView = UIView()
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableView.automaticDimension
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit,
                                                           target: self,
                                                           action: #selector(handleEditButton))
    }
    
    func prepareLabel() {
        noRatesLabel.text = """
            You don't have any rates yet. \
            You can add rates to this list \
            pressing + button
            """
        noRatesLabel.textColor = .gray
        noRatesLabel.textAlignment = .center
        noRatesLabel.lineBreakMode = .byWordWrapping
        noRatesLabel.numberOfLines = 0
        self.tableView.backgroundView?.addSubview(noRatesLabel)
        
        noRatesLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().inset(16)
            make.right.equalToSuperview().inset(16)
        }
    }
    
    func preapreButtons() {
        addButton.target = self
        addButton.action = #selector(handleAddButton)
    }
}

// MARK: - UITalbeViewDelegate
extension RatesViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rateViewModel.userRates?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rateCell = tableView.dequeueReusableCell(withIdentifier: "rateCell", for: indexPath) as! RateCell
        rateCell.userRate = rateViewModel.userRates?[indexPath.row]
        
        return rateCell
    }
}


// MARK: - UITableViewDelegate
extension RatesViewController {
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if let rate = rateViewModel.userRates?[indexPath.row] {
                rateViewModel.remove(rate: rate)
                tableView.deleteRows(at: [indexPath], with: .automatic)
         
                showLabelIfNeeded()
            }
        }
    }
}
