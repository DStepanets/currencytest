//
//  CurrencyViewModel.swift
//  CurrencyTest
//
//  Created by Dmitry Stepanets on 12/07/2019.
//  Copyright © 2019 Dmitry Stepanets. All rights reserved.
//

import UIKit
import RealmSwift

private let kCurrencies = ["AUD" : "Australian dollar",
                           "BGN" : "Bulgarian lev",
                           "BRL" : "Brazilian real",
                           "CAD" : "Canadian dollar",
                           "CHF" : "Swiss franc",
                           "CNY" : "Renminbi (Chinese) yuan",
                           "CZK" : "Czech koruna",
                           "DKK" : "Danish krone",
                           "EUR" : "Euro",
                           "GBP" : "Pound sterling",
                           "HKD" : "Hong Kong dollar",
                           "HRK" : "Croatian kuna",
                           "HUF" : "Hungarian forint",
                           "IDR" : "Indonesian rupiah",
                           "ILS" : "Israeli new shekel",
                           "INR" : "Indian rupee",
                           "ISK" : "Icelandic króna",
                           "JPY" : "Japanese yen",
                           "KRW" : "South Korean won",
                           "MAD" : "Moroccan dirham",
                           "MXN" : "Mexican peso",
                           "MYR" : "Malaysian ringgit",
                           "NOK" : "Norwegian krone",
                           "NZD" : "New Zealand dollar",
                           "PHP" : "Philippine peso",
                           "PLN" : "Polish złoty",
                           "RON" : "Romanian leu",
                           "RUB" : "Russian ruble",
                           "SEK" : "Swedish krona",
                           "SGD" : "Singapore dollar",
                           "THB" : "Thai baht",
                           "USD" : "United States dollar",
                           "ZAR" : "South African rand"]

class CurrencyViewModel: NSObject {
    //Private
    private let ratesProvider = RatesProvider()
    
    //Public
    private(set) var currencies:Results<CurrencySymbol>?
    var baseCurrencyCode:String = ""
    var targetCurrencyCode:String = ""
    
    override init() {
        super.init()
        
        fillCurrenciesIfNeeded()
        
        do {
            let realm = try Realm()
            self.currencies = realm.objects(CurrencySymbol.self).sorted(byKeyPath: "code", ascending: true)
        }
        catch {
            print("[CURRENCIES VIEW MODEL] \(error.localizedDescription)")
        }
    }
    
    func saveRate(completion:@escaping (_ error:String?) -> Void) {
        do {
            let realm = try Realm()
            guard
                let baseCurrency = realm.object(ofType: CurrencySymbol.self, forPrimaryKey: baseCurrencyCode),
                let targetCurrency = realm.object(ofType: CurrencySymbol.self, forPrimaryKey: targetCurrencyCode)
                else {
                    completion("Failed to initialize currencies")
                    return
            }
            
            let userRate = UserRate(withBaseRate: baseCurrency,
                                    targetCurrency: targetCurrency,
                                    amount: 0.0)
            try realm.write {
                realm.add(userRate, update: .all)
            }
            
            completion(nil)
        }
        catch {
            completion(error.localizedDescription)
        }
    }
    
    private func fillCurrenciesIfNeeded() {
        do {
            let realm = try Realm()
            if realm.objects(CurrencySymbol.self).isEmpty {
                var currencies = [CurrencySymbol]()
                for (key, value) in kCurrencies {
                    let symbol = CurrencySymbol(withCode: key, name: value)
                    currencies.append(symbol)
                }
                
                try realm.write {
                    realm.add(currencies)
                }
            }
        }
        catch {
            print("[CURRENCIES VIEW MODEL] \(error.localizedDescription)")
        }
    }
}
