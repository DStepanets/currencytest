//
//  RatesViewModel.swift
//  CurrencyTest
//
//  Created by Dmitry Stepanets on 12/07/2019.
//  Copyright © 2019 Dmitry Stepanets. All rights reserved.
//

import UIKit
import RealmSwift

protocol RatesViewModelDelegate: ViewModelDelegate {
    func ratesUpdateFinished()
}

class RatesViewModel:NSObject, ViewModelProtocol {
    //Private
    private let ratesProvider = RatesProvider()
    private var notificationToken:NotificationToken?
    private let dispatchGroup = DispatchGroup()
    
    //Public
    private(set) var userRates:Results<UserRate>?
    weak var delegate:RatesViewModelDelegate?
    
    deinit {
        self.notificationToken?.invalidate()
    }
    
    override init() {
        super.init()
        
        //Get saved currencies
        do {
            let realm = try Realm()
            self.userRates = realm.objects(UserRate.self)
            self.notificationToken = self.userRates?.observe({[weak self] (changes) in
                switch changes {
                case .update(_, deletions: _, let ins, modifications: _):
                    if let strongSelf = self {
                       self?.delegate?.viewModelDidChange(model: strongSelf)
                    }
                    
                    //Update rates if new record was added
                    if !ins.isEmpty {
                        self?.updateRates()
                    }
                    
                    break
                default:
                    break
                }
            })
        }
        catch {
            self.delegate?.viewModel(model: self, errorHasOccured: error.localizedDescription)
        }
    }
    
    func updateRates() {
        guard let rates = self.userRates else {
            return
        }
        
        var ratesDict = [String:[String]]()
        let allBases = rates.compactMap{$0.baseCurrency?.code ?? ""}
        for base in allBases {
            if base.isEmpty {
                continue
            }
            
            var targets = [String]()
            for rate in rates {
                if let baseRate = rate.baseCurrency {
                    if base == baseRate.code {
                        if let target = rate.toCurrency {
                            targets.append(target.code)
                        }
                    }
                }
            }
            targets.removeDuplicates()
            ratesDict[base] = targets
        }
        
        for (key, value) in ratesDict {
            dispatchGroup.enter()
            getRate(forBase: key, targetCodes: value)
        }
        
        dispatchGroup.notify(queue: DispatchQueue.main) {[weak self] in
            self?.delegate?.ratesUpdateFinished()
        }
    }
    
    func remove(rate:UserRate) {
        do {
            let realm = try Realm()
            if let objectToDelete = realm.object(ofType: UserRate.self, forPrimaryKey: rate.userRateId) {
                realm.beginWrite()
                realm.delete(objectToDelete)
                if let token = self.notificationToken {
                    try realm.commitWrite(withoutNotifying: [token])
                } else {
                    try realm.commitWrite()
                }
            }
        }
        catch {
            delegate?.viewModel(model: self, errorHasOccured: error.localizedDescription)
        }
    }
    
    func getRate(forBase baseCode:String, targetCodes:[String]) {
        self.ratesProvider.getLatestRates(forBaseCurrency: baseCode, targetCurrencies: targetCodes) {[weak self] (error) in
            self?.dispatchGroup.leave()
            
            if let error = error {
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.delegate?.viewModel(model: strongSelf, errorHasOccured: error)
            }
        }
    }
}
