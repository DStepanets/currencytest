//
//  ViewModelProtocol.swift
//  AviasalesTest
//
//  Created by Dmitry Stepanets on 05/07/2019.
//  Copyright © 2019 Dynamix Software. All rights reserved.
//

import UIKit

protocol ViewModelDelegate : class {
    func viewModelDidChange<P: ViewModelProtocol>(model:P)
    func viewModel<P: ViewModelProtocol>(model:P, errorHasOccured error:String?)
}

//Makes delegate methods optional
extension ViewModelDelegate {
    func viewModelDidChange<P: ViewModelProtocol>(model:P) {}
    func viewModel<P: ViewModelProtocol>(model:P, errorHasOccured error:String?) {}
}

protocol ViewModelProtocol {
}
